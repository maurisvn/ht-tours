<?php
// @codingStandardsIgnoreStart
if ( ! defined( 'FW' ) ) die( 'Forbidden' );

class FW_Extension_Tour extends FW_Extension{
    /*POST TYPE*/
	private $post_type      = 'ht_tour';
	private $post_type_slug = 'tour';

    /*CATEGORY*/
    private $category_name = 'ht_tour_cat';
    private $category_slug = 'tour-category';

    /*INIT*/
    public function _init(){
        $this->ht_define_slugs();

        add_action( 'init', array( $this, 'ht_action_register_post_type' ) );
        add_action( 'init', array( $this, 'ht_action_register_category' ) );

        $this->ht_add_options();

        if ( is_admin() ) {
			$this->save_permalink_structure();
			$this->add_admin_actions();
		}
    }

    private function add_admin_actions() {
		add_action( 'admin_init', array( $this, 'tour_permalink_section' ) );
	}

	public function tour_permalink_section() {
	    add_settings_field(
	        'goto_tour_base',
	        esc_html__( 'Tour Base', 'goto' ),
	        array( $this, 'tour_base_input_field' ),
	        'permalink',
	        'optional'
	    ); 

	    add_settings_field(
	        'goto_tour_category_base',
	        esc_html__( 'Tour Category Base', 'goto' ),
	        array( $this, 'tour_category_base_input_field' ),
	        'permalink',
	        'optional'
	    );
	}

	public function tour_base_input_field() {
	    ?>
	    <input type="text" name="goto_tour_base" value="<?php echo esc_attr( $this->post_type_slug ); ?>" />
	    <code>/your-tour-permalink</code>
	    <?php
	}

	public function tour_category_base_input_field() {
	    ?>
	    <input type="text" name="goto_tour_category_base" value="<?php echo esc_attr( $this->category_slug ); ?>" />
	    <code>/your-tour-category-permalink</code>
	    <?php
	}

    /*DEFINE SLUGS*/
    private function ht_define_slugs(){
        $this->post_type_slug = apply_filters(
			'goto_tour_base_slug',
			$this->get_db_data( 'permalinks/post', $this->post_type_slug )
		);

		$this->category_slug = apply_filters(
			'goto_tour_category_slug',
			$this->get_db_data( 'permalinks/taxonomy', $this->category_slug )
		);
    }

    private function save_permalink_structure() {

		if ( ! isset( $_POST['permalink_structure'] ) && ! isset( $_POST['category_base'] ) ) {
			return;
		}

		$post = FW_Request::POST( 'goto_tour_base',
			apply_filters( 'goto_tour_base_slug', $this->post_type_slug )
		);

		$taxonomy = FW_Request::POST( 'goto_tour_category_base',
			apply_filters( 'goto_tour_category_slug', $this->category_slug )
		);


		$this->set_db_data( 'permalinks/post', $post );
		$this->set_db_data( 'permalinks/taxonomy', $taxonomy );
	}

    /*ADD OPTIONS FOR TOUR*/
    public function ht_add_options(){
        add_filter( 'fw_post_options', array( $this, 'ht_tour_options' ), 10, 2 );
    }

    /* POST TYPE
    ***************************************************/
    public function ht_action_register_post_type(){
        $post_names = array(
            'singular' => __( 'Tour', 'haintheme' ),
            'plural'   => __( 'Tours', 'haintheme' )
        );

        $args = array(
            'labels'             => array(
                'name'               => $post_names['plural'],
                'singular_name'      => $post_names['singular'],
                'add_new'            => sprintf( __( 'Add New %s', 'haintheme' ), $post_names['singular'] ),
                'add_new_item'       => sprintf( __( 'Add New %s', 'haintheme' ), $post_names['singular'] ),
                'edit'               => __( 'Edit', 'haintheme' ),
                'edit_item'          => sprintf( __( 'Edit %s', 'haintheme' ), $post_names['singular'] ),
                'new_item'           => sprintf( __( 'New %s', 'haintheme' ), $post_names['singular'] ),
                'all_items'          => sprintf( __( 'All %s', 'haintheme' ), $post_names['plural'] ),
                'view'               => sprintf( __( 'View %s', 'haintheme' ), $post_names['singular'] ),
                'view_item'          => sprintf( __( 'View %s', 'haintheme' ), $post_names['singular'] ),
                'search_items'       => sprintf( __( 'Search %s', 'haintheme' ), $post_names['plural'] ),
                'not_found'          => sprintf( __( 'No %s Found', 'haintheme' ), $post_names['plural'] ),
                'not_found_in_trash' => sprintf( __( 'No %s Found In Trash', 'haintheme' ), $post_names['plural'] ),
                'parent_item_colon'  => '' /* text for parent types */
            ),
            'description'        => sprintf( __( 'Create a %s item', 'haintheme' ), $post_names['singular'] ),
            'public'             => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'show_in_nav_menus'     => true,
            'publicly_queryable' => true,
            /* queries can be performed on the front end */
            'has_archive'        => true,
            'rewrite'            => array( 'slug' => $this->post_type_slug ),
            'menu_position'      => 6,
            'show_in_nav_menus'  => true,
            'menu_icon'          => 'dashicons-welcome-write-blog',
            'hierarchical'       => false,
            'query_var'          => true,
            'show_in_rest'       => true,
            'supports'           => array(
                'title', /* Text input field to create a post title. */
                'editor',
                'comments',
                'excerpt',
                'thumbnail', /* Displays a box for featured image. */
            ),
            'capabilities'       => array(
                'edit_post'              => 'edit_pages',
                'read_post'              => 'edit_pages',
                'delete_post'            => 'edit_pages',
                'edit_posts'             => 'edit_pages',
                'edit_others_posts'      => 'edit_pages',
                'publish_posts'          => 'edit_pages',
                'read_private_posts'     => 'edit_pages',
                'read'                   => 'edit_pages',
                'delete_posts'           => 'edit_pages',
                'delete_private_posts'   => 'edit_pages',
                'delete_published_posts' => 'edit_pages',
                'delete_others_posts'    => 'edit_pages',
                'edit_private_posts'     => 'edit_pages',
                'edit_published_posts'   => 'edit_pages',
            ),
        );

        register_post_type( $this->post_type, $args );
    }

    public function get_post_type_name() {
        return $this->post_type;
    }

    /* CATEGORY
    ***************************************************/
    public function ht_action_register_category() {
        $category_names = array(
            'singular' => __( 'Tours Category', 'haintheme' ),
            'plural'   => __( 'Tour Categories', 'haintheme' )
        );

        $args = array(
            'labels'            => array(
                'name'              => $category_names['plural'],
                'singular_name'     => $category_names['singular'],
                'add_new'           => sprintf( __( 'Add New %s', 'haintheme' ), $category_names['singular'] ),
                'add_new_item'      => sprintf( __( 'Add New %s', 'haintheme' ), $category_names['singular'] ),
                'search_items'      => sprintf( __( 'Search %s', 'haintheme' ), $category_names['plural'] ),
                'all_items'         => sprintf( __( 'All %s', 'haintheme' ), $category_names['plural'] ),
                'parent_item'       => sprintf( __( 'Parent %s', 'haintheme' ), $category_names['singular'] ),
                'parent_item_colon' => sprintf( __( 'Parent %s:', 'haintheme' ), $category_names['singular'] ),
                'edit_item'         => sprintf( __( 'Edit %s', 'haintheme' ), $category_names['singular'] ),
                'update_item'       => sprintf( __( 'Update %s', 'haintheme' ), $category_names['singular'] ),
                'add_new_item'      => sprintf( __( 'Add New %s', 'haintheme' ), $category_names['singular'] ),
                'new_item_name'     => sprintf( __( 'New %s Name', 'haintheme' ), $category_names['singular'] ),
                'menu_name'         => $category_names['plural'],
            ),
            'show_in_rest'      => true,
            'public'            => true,
            'hierarchical'      => true,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'show_in_nav_menus' => true,
            'show_tagcloud'     => false,
            'rewrite'           => array( 'slug' => $this->category_slug ),
        );

        register_taxonomy( $this->category_name, $this->post_type, $args );
    }

    public function get_category_name() {
        return $this->category_name;
    }

    /* TOUR POST OPTIONS
    ***************************************************/
    public function ht_tour_options( $options, $post_type ){
        if( $post_type === $this->post_type ){
            // $options[] = array(
            //     $this->get_options( 'posts/' . $post_type, $options = array() )
            // );
            $options[] = apply_filters('custom_tour_option', array(
                    $this->get_options( 'posts/' . $post_type, $options = array() )
                ) 
            );
        }

        return $options;
    }
}