<?php

if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$manifest = array();

$manifest['name']        = esc_html__( 'Tours', 'haintheme' );
$manifest['description'] = esc_html__( 'Create custom post type Tours', 'haintheme' );
$manifest['bitbucket']   = 'maurisvn/ht-tours';
$manifest['author']      = 'maurisvn';
$manifest['version']     = '1.2.2';
$manifest['display']     = true;
$manifest['standalone']  = true;
$manifest['thumbnail']   = 'fa fa-plane';
