<?php

if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$tour_path = fw()->extensions->get( 'tour' )->get_uri();

$dropdown_data = array();
if ( defined( 'WPCF7_PLUGIN' ) ) {
	$args = array(
		'post_type'      => 'wpcf7_contact_form',
		'posts_per_page' => - 1,
	);

	$data = get_posts( $args );

	if ( $data ) {
		$dropdown_data[0] = esc_html__( 'Select your contact form', 'goto' );
		foreach ( $data as $key ) {
			$dropdown_data[ $key->ID ] = $key->post_title;
		}
	} else {
		$dropdown_data[0] = esc_html__( 'No contact form found', 'goto' );
	}
}

// get rating average on front-end.
$rating       = function_exists( 'fw_ext_feedback_stars_get_post_rating' ) ? fw_ext_feedback_stars_get_post_rating() : array();
$rating_value = isset( $rating['average'] ) ? intval( $rating['average'] ) : 0;

$tour_options = array(
	/* GENERAL
	***************************************************/
	't_general'      => array(
		'type'    => 'tab',
		'title'   => esc_html__( 'General', 'goto' ),
		'options' => array(
			// rating average.
			'rating'      => array(
				'type'       => 'hidden',
				'value'      => $rating_value,
				'fw-storage' => array(
					'type'      => 'post-meta',
					'post-meta' => 'rating',
				),
			),
			/*style*/
			'style'      => array(
				'type'    => 'image-picker',
				'label'   => esc_html__( 'Style', 'goto' ),
				'desc'    => esc_html__( 'Select your header style', 'goto' ),
				'choices' => array(
					'default' => $tour_path . '/static/images/default.png',
					'tour-1'  => array(
						'small' => $tour_path . '/static/images/s1.jpg',
						'large' => $tour_path . '/static/images/v1.jpg',
					),
					'tour-2'  => array(
						'small' => $tour_path . '/static/images/s2.jpg',
						'large' => $tour_path . '/static/images/v2.jpg',
					),
					'tour-3'  => array(
						'small' => $tour_path . '/static/images/s3.jpg',
						'large' => $tour_path . '/static/images/v3.jpg',
					),
				),
				'value'   => 'default',
			),

			/*price*/
			'price'      => array(
				'type'       => 'short-text',
				'label'      => esc_html__( 'Regular price', 'goto' ),
				'value'      => 100,
				'fw-storage' => array(
					'type'      => 'post-meta',
					'post-meta' => 'price',
				),
			),
			'sale_price' => array(
				'type'  => 'short-text',
				'label' => esc_html__( 'Sale price', 'goto' ),
				'desc'  => esc_html__( 'Please enter in a value less than the regular price.', 'goto' ),
			),

			/*gallery*/
			'gallery'    => array(
				'label' => esc_html__( 'Gallery', 'goto' ),
				'type'  => 'multi-upload',
			),

			/*video url*/
			'video_url'  => array(
				'label' => esc_html__( 'Video URL', 'goto' ),
				'desc'  => esc_html__( 'Example: https://youtube.com/watch?v=qDvFdj-pFMc', 'goto' ),
				'type'  => 'text',
				'value' => '',
			),

			/*map*/
			'tour_map'   => array(
				'label' => esc_html__( 'Google maps', 'goto' ),
				'desc'  => sprintf(
					esc_html__( 'Create a map %s', 'goto' ),
					'<a href="//www.google.com/maps/d/mp?hl=en&authuser=0&state=create" target="_blank">' . esc_html__( 'here', 'goto' ) . '</a>'
				),
				'type'  => 'textarea',
				'value' => '',
			),
		),
	),


	/* INFORMATION
	***************************************************/
	'short_info'     => array(
		'type'    => 'tab',
		'title'   => esc_html__( 'Information', 'goto' ),
		'options' => array(
			'days'       => array(
				'label'      => esc_html__( 'Days', 'goto' ),
				'type'       => 'text',
				'value'      => 6,
				'fw-storage' => array(
					'type'      => 'post-meta',
					'post-meta' => 'days',
				),
			),
			'date'       => array(
				'label'           => esc_html__( 'Departure Date', 'goto' ),
				'desc'            => esc_html__( 'Pick a date', 'goto' ),
				'type'            => 'datetime-picker',
				'value'           => '',
				'format'          => 'Y/m/d',
				'datetime-picker' => array(
					'format'     => 'Y/m/d',
					'maxDate'    => false,
					'minDate'    => false,
					'timepicker' => false,
					'datepicker' => true,
				),
				'fw-storage'      => array(
					'type'      => 'post-meta',
					'post-meta' => 'start_date',
				),
			),
			'start_time' => array(
				'label' => esc_html__( 'Departure Time', 'goto' ),
				'desc'  => esc_html__( 'Enter the departure time, ex: Every day', 'goto' ),
				'type'  => 'text',
				'value' => '',
			),
			'avaibility' => array(
				'label'      => esc_html__( 'Availability', 'goto' ),
				'type'       => 'short-text',
				'value'      => 30,
				'fw-storage' => array(
					'type'      => 'post-meta',
					'post-meta' => 'avaibility',
				),
			),
			'age'        => array(
				'label'      => esc_html__( 'Age', 'goto' ),
				'type'       => 'short-text',
				'value'      => 12,
				'fw-storage' => array(
					'type'      => 'post-meta',
					'post-meta' => 'age',
				),
			),
		),
	),


	/* ITINERARY
	***************************************************/
	'tour_itinerary' => array(
		'type'    => 'tab',
		'title'   => esc_html__( 'Itinerary', 'goto' ),
		'options' => array(
			'pdf'    => array(
				'type'      => 'upload',
				'label'     => esc_html__( 'File upload', 'goto' ),
				'files_ext' => array( 'txt', 'doc', 'docx', 'pdf', 'zip', 'rar' ),
				'desc'      => esc_html__( 'Available file type: txt, doc, docx, pdf, zip, rar', 'goto' ),
			),
			'detail' => array(
				'type'            => 'addable-box',
				'label'           => esc_html__( 'Itinerary', 'goto' ),
				'box-options'     => array(
					'thumb'   => array(
						'type'  => 'upload',
						'label' => esc_html__( 'Image', 'goto' ),
					),
					'title'   => array(
						'type'  => 'text',
						'label' => esc_html__( 'Enter the title', 'goto' ),
					),
					'content' => array(
						'type'  => 'wp-editor',
						'label' => esc_html__( 'Enter the content', 'goto' ),
					),
				),
				'template'        => '{{- title }}',
				'add-button-text' => esc_html__( 'Add', 'goto' ),
				'sortable'        => true,
			),
		),
	),


	/* MENU
	***************************************************/
	'tour_menu'      => array(
		'title'   => esc_html__( 'Menu', 'goto' ),
		'type'    => 'tab',
		'options' => array(
			'menu_layout' => array(
				'label'   => esc_html__( 'Layout', 'goto' ),
				'type'    => 'short-select',
				'choices' => array(
					'default'  => esc_html__( 'Default', 'goto' ),
					'layout-1' => esc_html__( 'Layout 1', 'goto' ),
					'layout-2' => esc_html__( 'Layout 2', 'goto' ),
					'layout-3' => esc_html__( 'Layout 3', 'goto' ),
				),
				'value'   => 'default',
			),
		),
	),

	/* BOOKING TYPE
	***************************************************/
	'tour_booking'   => array(
		'title'   => esc_html__( 'Booking Type', 'goto' ),
		'type'    => 'tab',
		'options' => array(
			'tour_booking_type' => array(
				'label'   => false,
				'type'    => 'multi-picker',
				'picker'  => array(
					'picked' => array(
						'label'   => esc_html__( 'Choose Tour Booking Type', 'goto' ),
						'type'    => 'select',
						'choices' => array(
							'default'     => esc_html__( 'Default', 'goto' ),
							'contact'     => esc_html__( 'Contact Form 7', 'goto' ),
							'woocommerce' => esc_html__( 'WooCommerce', 'goto' ),
							'shortcode' => esc_html__( 'Shortcode', 'goto' ),
						),
						'value'   => 'default',
					),
				),
				'choices' => array(
					'contact' => array(
						'ctf7' => array(
							'label'   => 'Choose Contact Form',
							'type'    => 'select',
							'choices' => $dropdown_data,
						),
					),
					'shortcode' => array(
						'shortcode' => array(
							'label'   => 'Input shortcode',
							'type'    => 'text',
							'desc' => 'For example: ["shortcode-A"]'
						)
					),
				),
			),
		),
	),
);

$options = array(
	'tour_layout_box' => array(
		'title'   => esc_html__( 'Tour Customizing', 'goto' ),
		'type'    => 'box',
		'options' => $tour_options,
	),
);
