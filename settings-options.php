<?php
// @codingStandardsIgnoreStart
if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$choices    = array();
$currencies = function_exists( 'goto_get_currencies' ) ? goto_get_currencies() : array();
if ( ! empty( $currencies ) && function_exists( 'goto_unit' ) ) {
	foreach ( $currencies as $code => $name ) {
		$symbol = goto_unit( $code );

		if ( $symbol === $code ) {
			/* translators: 1: currency name 2: currency code */
			$choices[ $code ] = html_entity_decode( sprintf( __( '%1$s (%2$s)', 'goto' ), $name, $code ) );
		} else {
			/* translators: 1: currency name 2: currency symbol, 3: currency code */
			$choices[ $code ] = html_entity_decode( sprintf( __( '%1$s (%2$s %3$s)', 'goto' ), $name, goto_unit( $code ), $code ) );
		}
	}
}

$dropdown_data = array();
if ( defined( 'WPCF7_PLUGIN' ) ) {
	$args = array(
		'post_type'      => 'wpcf7_contact_form',
		'posts_per_page' => -1,
		'post_status'    => 'publish',
	);

	$data = get_posts( $args );
	if ( empty( $data ) ) {
		$dropdown_data[0] = esc_html__( 'No contact form found', 'goto' );
	} else {
		$dropdown_data[0] = esc_html__( 'Select your contact form', 'goto' );
		foreach ( $data as $key ) {
			$dropdown_data[ $key->ID ] = $key->post_title;
		}
	}
}

$options = array(
	'box' => array(
		'type'    => 'box',
		'title'   => false,
		'options' => array(
			'unit'    => array(
				'label'   => esc_html__( 'Price unit', 'goto' ),
				'type'    => 'select',
				'choices' => $choices,
				'value'   => 'USD'
			),
			'symbol-pos' => array(
				'type'  => 'select',
				'value' => 'left',
				'label' => __('Symbol Position', '{domain}'),
				'desc'  => __('Change the currency symbol position. Default is left', 'goto'),
				'choices' => array(
					'left' => __('Left', 'goto'),
					'right' => __('Right', 'goto'),
				),
			),
			'character' => array(
				'type'  => 'text',
				'value' => ',',
				'label' => __('Character', '{domain}'),
			),
			'tour_booking_type' => array(
				'label'   => false,
				'type'    => 'multi-picker',
				'picker'  => array(
					'picked' => array(
						'label'   => esc_html__( 'Choose Tour Booking Type', 'goto' ),
						'type'    => 'select',
						'choices' => array(
							'contact'     => esc_html__( 'Contact Form 7', 'goto' ),
							'woocommerce' => esc_html__( 'WooCommerce', 'goto' ),
							'shortcode' => esc_html__( 'Shortcode', 'goto' ),
						),
						'value'   => 'woocommerce',
					),
				),
				'choices' => array(
					'contact' => array(
						'ctf7' => array(
							'label'   => 'Choose Contact Form',
							'type'    => 'select',
							'choices' => $dropdown_data,
						),
					),
					'shortcode' => array(
						'shortcode' => array(
							'label'   => 'Input shortcode',
							'type'    => 'text',
							'desc' => 'For example: ["shortcode-A"]'
						)
					),
				),
			),
		)
	)
);
